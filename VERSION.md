### Version

|Date|Version|What has been done|What going to add|
|-|-|-|-|
|2023-05-15|__v1.0.0__|the first version||
|2023-05-21|__v1.0.1__|fixed bug of json data to form data||
|2023-07-22|__v1.1.0__|support labii sdk core||
|2023-08-10|__v1.1.1__|support api key table||
|2023-11-05|__v1.1.2__|fixed a few bugs||
|2024-03-06|__v1.1.3__|updated applications (for v9)||
|2024-06-01|__v1.2.0__|support indexed db||
|2024-06-20|__v1.2.1__|add function to update tables.json||
|2024-07-05|__v1.2.2__|updated sdk git function||
|2024-10-07|__v2.0.0__|support async function||
|2024-11-09|__v2.1.0__|support api key for auth||
