# labii-sdk-js

A javascript SDK to handle Labii API calls

## INSTALL
1. Enable the virtual env: `python3 -m venv env`
2. Install the required packages: `pip install -r requirements.txt`

## API
- The get_list_url method takes props as an argument, which is an object containing properties like the app, model, and other parameters. It returns a URL string for the API to request a list.
- The get_detail_url method also takes props as an argument and returns a URL string for the API to request details for a specific item.
- The post method takes props as an argument, which is an object containing data to create a new item. It also accepts optional success and failure callbacks for handling API responses.
- The patch method is similar to post, but it is used to update a specific item.
- The get method accepts the URL to request API data and optional success and failure callbacks.
- The delete method is used to delete a specific item.

### Todo
- make sure data center is available