/**
 * update document title
 */
export const updateDocumentTitle = (title)=>{
    document.title = `${Title(title)} | Labii`;
}

// capital the first letter
export const Title = (data) => {
    if (!isEmpty(data)){
        var str = data.replaceAll("_", " ");
        return str.split(" ").map((s)=>{
            if (s === s.toUpperCase()){// keep the all upcase
                return s;
            }
            else{
                return s.charAt(0).toUpperCase() + s.slice(1);
            }
        }).join(' ');
    }
    else{
        return "";
    }
};

// truncate the string
export const truncateString = (str, num) => {
    if (str.length > num){
        return str.slice(0, num > 3 ? num - 3 : num) + '...';
    }
    else{
        return str;
    }
}

// random id
export const randomID = (count=18)=>{
    var text = "A";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < count; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

/**
 * return a letter from the number
 * 1 -> A
 * int i, 1-index
 */
export const numberToLetter = (i)=>{
    return String.fromCharCode(97 + i).toUpperCase()
}

/**
 * check has key
 */
export const hasKey = (object, key)=>{
    if (object === undefined || object === null){
        return false;
    }
    else{
        if (Object.keys(object).includes(key)){
            return true;
        }
        else{
            return false;
        }
    }
}

/**
 * check if the value is empty
 */
export const isEmpty = (data)=>{
    if (data === ""){
        return true;
    }
    else if (data === null){
        return true;
    }
    else if (data === undefined){
        return true;
    }
    else if (Array.isArray(data) && data.length === 0){
        return true;
    }
    else if (typeof(data) === "object" && Object.keys(data).length === 0){
        return true;
    }
    else{
        return false;
    }
}

/**
 * load an external script
 * url(str): the script url, use `/static/${this.version}/js/marvinjslauncher.js` if local
 * callback(func): onload function to call back
 */
export const loadScript = (url, callback=null)=>{
    const script = document.createElement("script");
    script.type='text/javascript';
    script.src = url;
    if (callback){
        script.onload = callback;
    }
    document.body.appendChild(script);
}

/**
 * load an external style sheet
 * url(str): the script url, use `/static/${this.version}/js/marvinjslauncher.js` if local
 */
export const loadStyle = (url)=>{
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.head.appendChild(link);
}

/**
 * to open a modal
 * str id, the modal id without #
 * is_modal_opened (state), check if modal is opened
 */
export const modalOpen = (id, is_modal_opened=null)=>{
    var elem = document.getElementById(id);
    //var instance = window.M.Modal.getInstance(elem);
    var instance = window.M.Modal.init(elem, {dismissible:false});
    instance.open();
    if (is_modal_opened !== null){
        is_modal_opened.set(true);
    }
}

/**
 * to close a modal
 * str id, the modal id without #
 */
export const modalClose = (id, is_modal_opened=null)=>{
    var elem = document.getElementById(id);
    var instance = window.M.Modal.getInstance(elem);
    instance.close();
    if (is_modal_opened !== null){
        is_modal_opened.set(false);
    }
}

/**
 * init the dropdown menu
 * - dropdown_id
 * - trigger-dropdown_id
 */
export const dropdownInit = (id, should_use_main_container=false)=>{
    var elem = document.getElementById(id);
    var options = {
        constrainWidth: false,
        coverTrigger: false
    }
    if (should_use_main_container){
        options["container"] = document.getElementById("main");
    }
    const instance = window.M.Dropdown.init(elem, options);
    return instance;
}

/**
 * return the time since
 */
export const timeSince =(datestring)=>{
    const intervals = [
        { label: 'year', seconds: 31536000 },
        { label: 'month', seconds: 2592000 },
        { label: 'day', seconds: 86400 },
        { label: 'hour', seconds: 3600 },
        { label: 'minute', seconds: 60 },
        { label: 'second', seconds: 1 }
    ];
    const date = new Date(datestring);
    if (date > new Date()){
        return datestring
    }
    else{
        var seconds = Math.floor((Date.now() - date.getTime()) / 1000);
        if (seconds < 1){
            seconds = 1;
        }
        const interval = intervals.find(i => i.seconds <= seconds);
        const count = Math.floor(seconds / interval.seconds);
        return `${count} ${interval.label}${count !== 1 ? 's' : ''} ago`;
    }
}

export const date2String = (d)=>{
    return `${d.getFullYear()}-${d.getMonth() <= 8 ? "0" : ""}${d.getMonth()+1}-${d.getDate() <= 9 ? "0" : ""}${d.getDate()}`;
}

/**
 * YYYY-MM-DD
 * YYYY-MM-DD HH:MM:SS
 */
export const string2Date = (d)=>{
    if (d.includes(" ")){
        var data = d.replace(" ", "-").replaceAll(":", "-").split("-");
        return new Date(data[0], data[1]-1, data[2], data[3], data[4], data.length === 6 ? data[5] : 0);
    }
    else{
        data = d.split("-");
        return new Date(data[0], data[1]-1, data[2]);
    }
}

/**
 * convert to json
 */
export const toJS = (data) =>{
    try{
        return JSON.parse(JSON.stringify(data));
    }
    catch{
        return JSON.parse(JSON.stringify(data.value));
    }
}

/**
 * run a function when an element is available
 */
export const waitForElement = (selector, callback) =>{
	const observer = new MutationObserver(function(mutations, me) {
		// query for the selector to check if the element is now part of the DOM
		var element = document.querySelector(selector);
		if (element) {
			// If the element is visible, call the callback and disconnect the observer
			if (element.offsetParent !== null) {
				callback();
				me.disconnect(); // stop observing
				return;
			}
		}
	});
	// Start observing the entire document
	observer.observe(document.documentElement, {
		childList: true,
		subtree: true
	});
}

/**
 * do a common toast
 */
export const toast = (data) =>{
    window.M.toast({html: data});
}

/**
 * do a common toast
 */
export const copy = (string, message="Copied") =>{
    navigator.clipboard.writeText(string);
    toast(message);
}

/**
 * format the file size
 */
export const formatBytes = (a, b) =>{if(0===a)return"0 Bytes";var c=1024, d=b||2, e=["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c, f)).toFixed(d))+" "+e[f]}

export const colors = [
    "#01579b", //blue
    "#009688", //teal
    "#e53935", //red
    "#4caf50", //grean
    "#e65100", //orange
    "#00bcd4", //cyan
    "#3f51b5", //indigo
    "#cddc39", //lime
    "#e91e63", //pink
    "#8bc34a", //light green
    "#9c27b0", //purple
    "#ffc107", //amber
    "#795548", //brown
];

export const backgroundColors = [
    "#b3e5fc", //blue
    "#b2dfdb", //teal
    "#ffcdd2", //red
    "#c8e6c9", //grean
    "#ffe0b2", //orange
    "#b2ebf2", //cyan
    "#c5cae9", //indigo
    "#f0f4c3", //lime
    "#f8bbd0", //pink
    "#dcedc8", //light green
    "#e1bee7", //purple
    "#ffecb3", //amber
    "#d7ccc8", //brown
];
