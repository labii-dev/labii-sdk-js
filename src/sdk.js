import {toast, hasKey, Title} from './utils';
import {API, checkRequiredQuery} from './api';
import tables from './tables.json';
import {IndexedDB} from './storage';
import {getObject} from './local-store';

export const getIndexedDBData = async (url, props)=>{
	const datacenter = getObject("datacenter");
	let idb_label = `${datacenter.domain_api}${url}`;
	let data = undefined
	if (performance.navigation.type !== 1){// fetch if reload
		if (hasKey(props, "idb_should_skip_load") && props.idb_should_skip_load === false){
			data = await IndexedDB.read(idb_label);
		}
	}
	return {idb_label, data};
}

export const updateIndexedDBData = async (idb_label, response, props)=>{
	// save to indexed db
	await IndexedDB.save(idb_label, response.data, props.idb_date_expiration ? props.idb_date_expiration : "");
}

/**
 * create
 * - data
 * - query=""
 * - callback_success=null
 * - callback_fail=null
 * modify
 * - sid
 * - data
 * delete
 * - sid
 * IndexedDB
 * - idb_should_skip_load
 * - idb_date_expiration
 */
export const APIResource = {
	create: async (props)=>{
		if (props.is_async){
			const response = await API.post_async(props); // Make an asynchronous POST request
			return response;
		}
		else{
			API.post(props); // Make a synchronous POST request
		}
	},
	list: async (props)=>{
		// check local indexed db
		let url = API.get_list_url(props);
		let {idb_label, data} = await getIndexedDBData(url, props);
		if (data !== undefined){
			if (props.is_async){
				return {data: data};
			}
			else if (props.callback_success){
				props.callback_success({data: data});
			}
		}
		else{// fetch data
			// all_pages=true to download all pages
			if (hasKey(props, "all_pages") && props.all_pages === true){
				if (!hasKey(props, "page")){
					props.page = 1;
				}
				if (!hasKey(props, "page_size")){
					props.page_size = 10;
				}
				if (!hasKey(props, 'data')){
					props.data = {};
				}
				// download
				if (props.is_async){
					const response = await API.get_async({
						...props,
						url: API.get_list_url(props),
					})
					if (hasKey(response.data, "results")){// success
						if (hasKey(props.data, "count")){
							props.data.results = props.data.results.concat(response.data.results);
						}
						else{
							props.data = response.data;
						}
						if (response.data.next !== null){
							props.page += 1;
							APIResource.list(props);
						}
						else{
							// save to indexed db
							await updateIndexedDBData(idb_label, {data: props.data}, props);
							return {data: props.data};
						}
					}
					else{
						return response;
					}
				}
				else{
					API.get({
						...props,
						url: API.get_list_url(props),
						callback_success: async (response)=>{
							if (hasKey(props.data, "count")){
								props.data.results = props.data.results.concat(response.data.results);
							}
							else{
								props.data = response.data;
							}
							if (response.data.next !== null){
								props.page += 1;
								APIResource.list(props);
							}
							else{
								// save to indexed db
								await updateIndexedDBData(idb_label, {data: props.data}, props);
								// call back
								if (props.callback_success){ // If a success callback is provided
									props.callback_success({data: props.data}); // Call the success callback with the response
								}
							}
						}
					})
				}
			}
			else{
				if (props.is_async){
					const response = await API.get_async({
						...props,
						url: url,
					})
					if (response){
						await updateIndexedDBData(idb_label, response, props);
					}
					return response;
				}
				else{
					API.get({
						...props,
						url: url,
						callback_success: async (response)=>{
							await updateIndexedDBData(idb_label, response, props);
							// call back
							if (props.callback_success){ // If a success callback is provided
								props.callback_success(response); // Call the success callback with the response
							}
						}
					})
				}
			}
		}
	},
	retrieve: async (props)=>{
		// describe props needed in this function block
		// - sid
		// - callback_success
		// - is_async
		// check local indexed db
		let url = API.get_detail_url(props);
		let {idb_label, data} = await getIndexedDBData(url, props); // Retrieve data from IndexedDB if available
		if (data !== undefined){ // If data is found in IndexedDB
			if (props.is_async){ // If the request is asynchronous
				return {data: data}; // Return the data
			}
			else if (props.callback_success){ // If a success callback is provided
				props.callback_success({data: data}); // Call the success callback with the data
			}
		}
		else{ // If data is not found in IndexedDB, fetch from the API
			if (props.is_async){ // If the request is asynchronous
				const response = await API.get_async({
					...props,
					url: API.get_detail_url(props),
				})
				await updateIndexedDBData(idb_label, response, props); // Save the fetched data to IndexedDB
				return response; // Return the response
			}
			else{ // If the request is synchronous
				API.get({
					...props,
					url: API.get_detail_url(props),
					callback_success: async (response)=>{
						await updateIndexedDBData(idb_label, response, props); // Save the fetched data to IndexedDB
						// call back
						if (props.callback_success){ // If a success callback is provided
							props.callback_success(response); // Call the success callback with the response
						}
					}
				})
			}
		}
	},
	modify: async (props)=>{
		// describe props needed in this function block
		// - sid
		// - callback_success
		// - is_async
		const datacenter = getObject("datacenter");
		let url = API.get_detail_url(props);
		let idb_label = `${datacenter.domain_api}${url}`;
		if (props.is_async){ // Check if the request should be asynchronous
			const response = await API.patch_async({
				...props,
				url: url
			}); // Make an asynchronous PATCH request
			await updateIndexedDBData(idb_label, response, props);
			return response;
		}
		else{ // If the request is synchronous
			API.patch({
				...props,
				url: url,
				callback_success: async (response)=>{
					await updateIndexedDBData(idb_label, response, props);
					// call back
					if (props.callback_success){ // If a success callback is provided
						props.callback_success(response);
					}
				}
			}); // Make a synchronous PATCH request
		}
	},
	delete: async (props)=>{
		// - sid
		// - callback_success
		// - callback_fail
		// - is_async
		const datacenter = getObject("datacenter");
		let url = API.get_detail_url(props);
		let idb_label = `${datacenter.domain_api}${url}`;
		if (props.is_async){ // Check if the request should be asynchronous
			const response = await API.delete_async({
				...props,
				url: url
			});
			await IndexedDB.remove(idb_label); // Remove data from IndexedDB
			return response;
		}
		else{
			API.delete({
				...props,
				url: url,
				callback_success: async ()=>{
					// clear the indexedDB
					await IndexedDB.remove(idb_label); // Remove data from IndexedDB
					if (props.callback_success){
						props.callback_success();
					}
					else{
						toast("Deleted!");
					}
				},
				callback_fail: (error)=>{
					if (props.callback_fail){
						props.callback_fail(error);
					}
					toast(error);
				}
			})
		}
	}
}

/**
 * prepare the Labii SDK
 */
var Labii = {};
// the query to check
const required_query = {
	//"rows": ["table__sid"],
	"signers": ["row__sid"]
}
// prepare the sdk
Object.keys(tables).forEach((table)=>{
	var name = Title(tables[table].name_singular);
	Labii[name] = {
		create: async (props)=>{
			if (hasKey(required_query, table)){
				checkRequiredQuery(props, required_query[table]);
			}
			var default_props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			props = {...default_props, ...props};
			if (props.is_async){
				const response = await APIResource.create(props);
				return response;
			}
			else{
				APIResource.create(props);
			}
		},
		list: async (props)=>{
			if (hasKey(required_query, table)){
				checkRequiredQuery(props, required_query[table]);
			}
			var default_props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			props = {...default_props, ...props};
			if (props.is_async){
				const response = await APIResource.list(props);
				return response;
			}
			else{
				APIResource.list(props);
			}
		},
		retrieve: async (props)=>{
			var default_props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			props = {...default_props, ...props};
			if (props.is_async){
				const response = await APIResource.retrieve(props);
				return response;
			}
			else{
				APIResource.retrieve(props);
			}
		},
		modify: async (props)=>{
			var default_props = {
				app: tables[table].app,
				model: tables[table].name_singular,
				callback_success: ()=>{
					toast("Updated!");
				}
			};
			props = {...default_props, ...props};
			if (props.is_async){
				const response = await APIResource.modify(props);
				return response;
			}
			else{
				APIResource.modify(props);
			}
		}
	}
	// delete
	if ((hasKey(tables[table], 'should_hide_delete_button') && tables[table].should_hide_delete_button === false) || ["signers"].includes(table)){
		Labii[name]["delete"] = async (props)=>{
			var default_props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			props = {...default_props, ...props};
			if (props.is_async){
				const response = await APIResource.delete(props);
				return response;
			}
			else{
				APIResource.delete(props);
			}
		}
	}
})

export default Labii;
