import {Local} from './storage';
import {hasKey, isEmpty} from './utils';

// keys
// selected - the select records
// next - the next url
// sidenav - the padding left value
// organization - the current organization
// project - the current project
// user - the current user
// session_idle - the idel time in seconds

/**
 * function to return an object
 * props
 * - type, type of store
 * - default, the default data when not assigned
 */
export const getObject = (key, props={})=>{
    var ob = {};
    if (Local.read(key) === null){
        if (hasKey(props, "default")){
            ob = props.default;
        }
    }
    else{
        ob = Local.read(key);
    }
    if (["tables", "projects", "selected"].includes(key)){
        if (!Array.isArray(ob)){
            ob = [];
        }
    }
    return ob;
}

/**
 * function to update the data
 * props
 * - key
 * - data
 */
export const updateObject = (key, props={})=>{
	if (!Object.keys(props).includes("data")){
		console.log("Error: Missing data key!")
	}
	else{
		Local.save(key, props.data);
	}
}

/**
 * function to clear the object
 * props
 * - key
 */
export const clearObject = (key)=>{
    Local.remove(key);
}

/**
 * function to save next store
 */
export const updateNextStore = ()=>{
    const datacenter = getObject("datacenter");
    var url = window.location.href;
    var excludes = ["/accounts/", "/organization", "/datacenter", "/print", ".app/?", "localhost:3000/?"];
    var equals = ["/"]
    var excluded = false;
    excludes.forEach((n)=>{
        if (url.includes(n)){
            excluded = true;
        }
    });
    equals.forEach((n)=>{
        if (url.replace(datacenter.domain_front, "") === n){
            excluded = true;
        }
    });
    if (url.split("/").length === 4 && url.split("/")[3] === ""){// avoid bug when cross domain usage
        excluded = true;
    }
    if (excluded === false){
        url = url.split("#!")[0]; // avoid the click url
        Local.save('next', {url: url});
    }
}

/**
 * function to redirect to next url
 * return true if successful
 */
export const redirectToNext = ()=>{
    const datacenter = getObject("datacenter");
    const next = getObject("next");
    Local.remove("next");
    if (!isEmpty(next.url) && next.url !== `${datacenter.domain_front}/`){
        window.location.assign(next.url);
        return true;
    }
    else{
        return false;
    }
}
