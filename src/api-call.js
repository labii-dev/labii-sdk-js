import {randomID, hasKey, isEmpty} from './utils';
import {getObject, clearObject, updateNextStore, updateObject} from './local-store';
import {Session} from './storage';
import axios from 'axios';

const prepareAxiosOptions = (props) => {
	const user = getObject("user");
	const api_key = Session.read("api_key");
	const datacenter = getObject("datacenter");
	// update the heart beat
	var x_labii_source = user.secret; // browser identify
	if (isEmpty(x_labii_source)){
		x_labii_source = randomID();
		updateObject("x_labii_source", {data: x_labii_source})
	}
	// get headers
	var Headers = {
		"Content-Type": "application/json",
		"X-Labii-Source": x_labii_source
	}
	if (!hasKey(props, "login_required") || props.login_required === true){
		if (user.token){
			Headers["Authorization"] = "Token " + user.token;
		}
		else if (api_key !== null){
			Headers["Authorization"] = "Bearer " + api_key;
		}
		else{
			updateNextStore();
			window.location.assign("/accounts/login/");
		}
	}
	if (hasKey(props, "headers") && !isEmpty(props.headers)){
		Headers = props.headers;
	}
	//action
	var options = {
		method: props.method ? props.method : 'get',
		url: props.url.split("#")[0],
		headers: Headers
	};
	["responseType", "file", "onUploadProgress"].forEach((o)=>{
		if (hasKey(props, o)){
			options[o] = props[o];
		}
	});
	// stringfy the data
	if (hasKey(props, "data")){
		options["data"] = JSON.stringify(props.data);
	}
	if (hasKey(props, "rawdata")){
		options["data"] = props.rawdata;
	}
	// url domain
	if (options["url"].substring(0, 4) !== "http"){
		options["url"] = `${datacenter.domain_api}${options["url"]}`;
	}
	// print log for debug
	if (datacenter.is_debug){
		// eslint-disable-next-line no-console
		console.log(`${options["method"]}: ${options["url"]}${["post", "patch"].includes(options["method"]) ? ": " : ""}${["post", "patch"].includes(options["method"]) ? options["data"] : ""}`); // for debug
	}
	// next store
	updateNextStore();
	return options;
}

/**
 * props
 * - login_required, default to true
 * - headers, other headers to add
 * - rawheaders, headers to replace
 * - method
 * - url
 * - responseType
 * - responseType
 * - onUploadProgress
 * - data
 * - rawdata
 */
export const makeApiCall = (props) => {
	const datacenter = getObject("datacenter");
	var options = prepareAxiosOptions(props);
	// start
	return axios(options)
	.then((response)=>{
		// reset the alive time
		updateObject("session_idle", {data: new Date().getTime()});
		if (props.success){
			return props.success(response);
		}
		else{
			if (datacenter.is_debug){
				// eslint-disable-next-line no-console
				console.log(response);
			}
		}
	})
	.catch((error)=>{
		if (props.catch){
			props.catch(error);
		}
		else{
			if (error.response !== undefined){
				if (error.response.status === 401){
					clearObject("user");
					window.location.assign("/accounts/login/");
				}
				else if (error.response.status === 500){
					//window.location.assign(`/error/404/?url=${window.location.href}`);
				}
				else{
					// find a method to detect wrong organization id
					if (JSON.stringify(error.response.data).indexOf("Wrong id") !== -1){
						clearObject("next");
						window.location.assign("/organizations/");
					}
				}
			}
			else{
				if (datacenter.is_debug){
					// eslint-disable-next-line no-console
					console.log(error);
				}
			}
			if (props.fail){
				var error_message = "Internal Server Error";
				if (hasKey(error.response, "data")){
					if (hasKey(error.response.data, "detail")){
						error_message = error.response.data.detail;
					}
					else if (hasKey(error.response.data, "message")){
						error_message = error.response.data.message;
					}
					else{
						error_message = JSON.stringify(error.response.data).split("\n")[0];
					}
				}
				// error_message = `${error_message}. Click Menu -> Switch Organization might resolve the error.`
				props.fail(error_message);
			}
		}
	});
}

makeApiCall.defaultProps = {
	login_required: true
}

export default makeApiCall;

export const makeApiCallAsync = async (props) => {
	const datacenter = getObject("datacenter");
	var options = prepareAxiosOptions(props);
    try {
        const response = await axios(options);
		// reset the alive time
		updateObject("session_idle", {data: new Date().getTime()});
		if (datacenter.is_debug){
			// eslint-disable-next-line no-console
			console.log(response);
		}
        return response; // Return the data if needed
    } catch (error) {
		if (error.response !== undefined){
			if (error.response.status === 401){
				clearObject("user");
				window.location.assign("/accounts/login/");
			}
			else if (error.response.status === 500){
				//window.location.assign(`/error/404/?url=${window.location.href}`);
			}
			else{
				// find a method to detect wrong organization id
				if (JSON.stringify(error.response.data).indexOf("Wrong id") !== -1){
					clearObject("next");
					window.location.assign("/organizations/");
				}
			}
		}
		else{
			if (datacenter.is_debug){
				// eslint-disable-next-line no-console
				console.log(error);
			}
			return error;
		}
    }
}

makeApiCallAsync.defaultProps = {
	login_required: true
}