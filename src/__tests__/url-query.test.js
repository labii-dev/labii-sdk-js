import {query2json, json2query, getURLQueryJson, updateURL, encodeURL, formData2Query, queryData2FormData} from '../url-query';

test("Testing query2json function", () => {
	const query = "first=sample&second=false&third=true&next=page#data";
	expect(query2json(query)).toEqual({ first: "sample", second: false, third: true, next: 'page#data' });
	const query2 = "first=Hello%20World&second=false&third=true#";
	expect(query2json(query2)).toEqual({ first: "Hello World", second: false, third: true });
	const query3 = "#data=page";
	expect(query2json(query3)).toEqual({});
  });

describe("json2query function", () => {
	it("should return an empty string when passed an empty object", () => {
		expect(json2query({})).toBe("");
	});

	it("should only include key-value pairs with non-empty values in the query string", () => {
		const json = { name: "John", age: 30, address: "" };
		const query = json2query(json);
		expect(query).toBe("name=John&age=30");
	});

	it("should JSON stringify nested objects", () => {
		const json = { name: "John", address: { city: "New York", country: "USA" } };
		const query = json2query(json);
		expect(query).toBe("name=John&address=%7B%22city%22%3A%22New%20York%22%2C%22country%22%3A%22USA%22%7D");
	});
});
  
describe('getURLQueryJson', () => {
	it('should return an empty object when there is no query string', () => {
		window.history.replaceState({}, '', '/')
		const query = getURLQueryJson()
		expect(query).toEqual({})
	})
  
	it('should convert query string to JSON object', () => {
		window.history.replaceState({}, '', '/?foo=bar&baz=123')
		const query = getURLQueryJson()
		expect(query).toEqual({
			foo: 'bar',
			baz: '123'
		})
	})
  
	it('should handle URL-encoded values', () => {
		window.history.replaceState({}, '', '/?encoded=%3Cscript%3Ealert(%22hello%22)%3B%3C%2Fscript%3E')
		const query = getURLQueryJson()
		expect(query).toEqual({
			encoded: '<script>alert("hello");<\/script>'
		})
	})
})

describe("updateURL", () => {
	test("returns the original URL when json object is empty", () => {
		const url = "https://example.com/test#page1";
		const json = {};
		expect(updateURL(url, json)).toBe(url.split("#")[0]);
	});
  
	test("appends query string to URL when no existing query string exists", () => {
		const url = "https://example.com/test#page1";
		const json = {param1: "value1", param2: "value2"};
		expect(updateURL(url, json)).toBe("https://example.com/test?param1=value1&param2=value2");
	});
  
	test("updates existing parameter and adds new parameter to URL", () => {
		const url = "https://example.com/test?param1=value1#page1";
		const json = {param1: "new_value", param2: "value2"};
		expect(updateURL(url, json)).toBe("https://example.com/test?param1=new_value&param2=value2");
	});
  
	test("handles URL without fragment identifier", () => {
		const url = "https://example.com/test?param1=value1";
		const json = {param2: "value2"};
		expect(updateURL(url, json)).toBe("https://example.com/test?param1=value1&param2=value2");
	});
});

describe("encodeURL", () => {
	it("should encode the given URL and replace '@' with '%40'", () => {
		const url = "https://example.com/@user";
		const encodedURL = encodeURL(url);
		expect(encodedURL).toBe("https://example.com/%40user");
	});
  
	it("should return the same URL if it does not contain '@'", () => {
		const url = "https://example.com/test";
		const encodedURL = encodeURL(url);
		expect(encodedURL).toBe(url);
	});
});

describe('formData2Query', () => {
	it('should properly convert data to query', () => {
		const data = {
			name: 'John Doe',
			age: 25,
			address: {
				street: 'Main street',
				city: 'New York'
			},
			hobbies: ['reading', 'swimming']
		};
		const expectedQuery = `{"name":"John Doe","age":25,"address":"{'street':'Main street','city':'New York'}","hobbies":"['reading','swimming']"}`
			.replaceAll('%', '%25')
			.replaceAll('?', '%3F')
			.replaceAll('&', 'and')
			.replaceAll('=', '__EQ__')
			.replaceAll("'", '%27')
			.replaceAll('"', '%22');
		const query = formData2Query(data);
		expect(query).toBe(expectedQuery);
	});
  
	it('should return an empty object if no data is provided', () => {
		const query = formData2Query({});
		expect(query).toBe('{}');
	});

	it('should properly handle "undefined" or "null" values', () => {
		const data = {
			name: 'John Doe',
			age: null,
			address: undefined
		};
		const expectedQuery = '{"name":"John Doe","age":"","address":""}'
			.replaceAll('%', '%25')
			.replaceAll('?', '%3F')
			.replaceAll('&', 'and')
			.replaceAll('=', '__EQ__')
			.replaceAll("'", '%27')
			.replaceAll('"', '%22');
		const query = formData2Query(data);
		expect(query).toBe(expectedQuery);
	});
});

describe('queryData2FormData', () => {
	it('should replace "__EQ__" with "=" in JSON string', () => {
		const input = '{"foo": "__EQ__bar"}';
		const expectedOutput = { "foo": "=bar" };
		const output = queryData2FormData(input);
		expect(output).toEqual(expectedOutput);
	});

	it('should convert nested JSON string into object', () => {
		const input = '{"foo": "{\\"bar\\": \\"baz\\"}"}';
		const expectedOutput = { "foo": { "bar": "baz" } };
		const output = queryData2FormData(input);
		expect(output).toEqual(expectedOutput);
	});
});
  
