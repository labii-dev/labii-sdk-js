import {makeApiCall} from '../api-call';
import { getObject, updateNextStore } from '../local-store';
import axios from "axios";

// Mock the functions used in the makeApiCall function
jest.mock("../utils", () => ({
	hasKey: jest.fn(),
	isEmpty: jest.fn(),
	randomID: jest.fn(),
}));

jest.mock("../local-store", () => ({
	getObject: jest.fn(),
	updateObject: jest.fn(),
	clearObject: jest.fn(),
	updateNextStore: jest.fn(),
}));

jest.mock('axios');

describe("makeApiCall", () => {
	let props;

	beforeAll(() => {
		// Define the props to be used in the tests
		props = {
			method: "get",
			url: "/test",
			login_required: true,
		};
	});

	afterEach(() => {
		// Reset the mocks after each test
		jest.resetAllMocks();
	});

	it("should call axios with the correct options", async () => {
		// Define the expected axios options
		const expectedOptions = {
			method: "get",
			url: "http://example.com/test",
			headers: {
				"Content-Type": "application/json",
				"X-Forwarded-For": "test-secret",
				Authorization: "Token test-token",
			},
		};

		// Mock the return values of the functions used in the makeApiCall function
		getObject.mockImplementation((key) => {
			if (key === "user") {
				return {
					secret: "test-secret",
					token: "test-token",
				};
			} else if (key === "datacenter") {
				return {
					domain_api: "http://example.com",
					is_debug: false,
				};
			}
		});

		const successData = {};
		axios.mockResolvedValue({});

		// Call the makeApiCall function with the defined props
		await makeApiCall(props);

		// Assert that axios was called with the expected options
		expect(axios).toHaveBeenCalledWith(expectedOptions);
	});

	it("should call the success callback if provided", async () => {
		// Define the expected response from the axios call
		const expectedResponse = {
			data: {},
		};

		// Define the success callback
		const successCallback = jest.fn();

		// Mock the return values of the functions used in the makeApiCall function
		getObject.mockImplementation((key) => {
			if (key === "user") {
				return {
					secret: "test-secret",
					token: "test-token",
				};
			} else if (key === "datacenter") {
				return {
					domain_api: "http://example.com",
					is_debug: false,
				};
			}
		});
		axios.mockResolvedValue(expectedResponse);

		// Update the props to include the success callback
		props.success = successCallback;

		// Call the makeApiCall function with the updated props
		await makeApiCall(props);

		// Assert that the success callback was called with the expected response
		expect(successCallback).toHaveBeenCalledWith(expectedResponse);
	});

	it("should call the fail callback if provided and there is an error", async () => {
		// Define the expected error message
		const expectedErrorMessage = "Internal Server Error";

		// Define the error response from the axios call
		const errorResponse = {
			response: {
				data: {
				detail: expectedErrorMessage,
				},
			},
		};

		// Define the fail callback
		const failCallback = jest.fn();

		// Mock the return values of the functions used in the makeApiCall function
		getObject.mockImplementation((key) => {
			if (key === "user") {
				return {
					secret: "test-secret",
					token: "test-token",
				};
			} else if (key === "datacenter") {
				return {
					domain_api: "http://example.com",
					is_debug: false,
				};
			}
		});
		axios.mockRejectedValue(errorResponse);

		// Update the props to include the fail callback
		props.fail = failCallback;

		// Call the makeApiCall function with the updated props
		await makeApiCall(props);

		// Assert that the fail callback was called with the expected error message 
		expect(failCallback).toHaveBeenCalledWith(expectedErrorMessage); 
	});

	it("should call the fail callback if provided and there is no error message", async () => { // Define the error response from the axios call const errorResponse = {};

		// Define the fail callback
		const failCallback = jest.fn();
		
		// Mock the return values of the functions used in the makeApiCall function
		getObject.mockImplementation((key) => {
			if (key === "user") {
				return {
					secret: "test-secret",
					token: "test-token",
				};
			} else if (key === "datacenter") {
				return {
					domain_api: "http://example.com",
					is_debug: false,
				};
			}
		});
		axios.mockRejectedValue({});
		
		// Update the props to include the fail callback
		props.fail = failCallback;
		
		// Call the makeApiCall function with the updated props
		await makeApiCall(props);
		
		// Assert that the fail callback was called with a default message
		expect(failCallback).toHaveBeenCalledWith("Internal Server Error");
		
		
	}); 
});
