import {getObject, updateObject, clearObject, updateNextStore, redirectToNext} from '../local-store';
import {Local} from '../storage';
import { JSDOM } from 'jsdom';

describe('getObject', () => {
    it("Test getObject function with valid key", () => {
        const key = "tables";
        const expectedOutput = [];
    
        const result = getObject(key);
    
        expect(result).toEqual(expectedOutput);
    });
    
    it("Test getObject function with invalid key", () => {
        const key = "invalidKey";
        const expectedOutput = {};
    
        const result = getObject(key);
    
        expect(result).toEqual(expectedOutput);
    });
    
    it("Test getObject function with default prop", () => {
        const key = "invalidKey";
        const defaultObject = { id: 1, name: "test" };
        const expectedOutput = defaultObject;
    
        const result = getObject(key, { default: defaultObject });
    
        expect(result).toEqual(expectedOutput);
    });
    
    it("Test getObject function with array key", () => {
        const key = "tables";
        const expectedOutput = [];
    
        const result = getObject(key);
    
        expect(Array.isArray(result)).toBeTruthy();
    });
})

describe('updateObject', () => {
    it('should save data to Local Storage', () => {
        const key = 'test-key';
        const props = { data: 'test-data' };
        
        updateObject(key, props);
        
        expect(getObject(key)).toEqual(props.data);
        localStorage.removeItem(key);
    });
  
    it('should save an empty object to Local Storage if no props argument is provided', () => {
        const key = 'test-key';
        
        updateObject(key);
        
        expect(localStorage.getItem(key)).toEqual(null);
        localStorage.removeItem(key);
    });
});

describe('clearObject', () => {
    it('should remove the specified key from Local', () => {
        const key = 'testKey';
        const spy = jest.spyOn(Local, 'remove');
        clearObject(key);
        expect(spy).toHaveBeenCalledWith(key);
        spy.mockRestore();
    });
});

describe("updateNextStore", () => {
    const mockGetObject = jest.fn();
    const mockLocalSave = jest.fn();
  
    beforeAll(() => {
        global.getObject = mockGetObject;
        global.window = Object.create(window);
        Object.defineProperty(window, 'location', {
            value: {
            href: "https://example.com/",
            }
        });
        global.Local = {
            save: mockLocalSave,
        }
    });
  
    afterAll(() => {
        delete global.getObject;
        delete global.window;
        delete global.Local;
    });
  
    afterEach(() => {
        jest.clearAllMocks();
    });
  
    it("should not update next store if excluded", () => {
        mockGetObject.mockReturnValue({ domain_front: "example.com" });
    
        // should exclude due to URL containing "/accounts/"
        window.location.href = "https://example.com/accounts/";
    
        updateNextStore();
    
        expect(mockLocalSave).not.toHaveBeenCalled();
    });
  
    it("should update next store if not excluded", () => {
        mockGetObject.mockReturnValue({ domain_front: "example.com" });
    
        // should not exclude due to URL not containing any of the excluded strings
        window.location.href = "https://example.com/path/to/page?query=string";
    
        updateNextStore();
        let next = getObject("next");
        expect(next.url).toEqual(window.location.href);
    });
  
    it("should exclude when URL is just domain with no path", () => {
        mockGetObject.mockReturnValue({ domain_front: "example.com" });
    
        // should exclude due to URL being equal to "/"
        window.location.href = "https://example.com/";
    
        updateNextStore();
    
        expect(mockLocalSave).not.toHaveBeenCalled();
    });
  
    it("should exclude when URL has 4 slashes", () => {
        mockGetObject.mockReturnValue({ domain_front: "example.com" });
    
        // should exclude due to URL containing 4 slashes
        window.location.href = "https://example.com/path/to/page/";
    
        updateNextStore();
    
        expect(mockLocalSave).not.toHaveBeenCalled();
    });
  
    it("should exclude when URL has anchor tag", () => {
        mockGetObject.mockReturnValue({ domain_front: "example.com" });
    
        // should exclude due to URL containing "#!" string
        window.location.href = "https://example.com/path/to/page/#!/anchor";
    
        updateNextStore();
    
        expect(mockLocalSave).not.toHaveBeenCalled();
    });
});


describe('redirectToNext function', () => {
    beforeEach(() => {
        localStorage.clear();
    });

    it('should not redirect and return false if there is no "next" object in localStorage', () => {
        const result = redirectToNext();
        expect(result).toBe(false);
    });

    it('should not redirect and return false if the "next" object in localStorage has an empty url', () => {
        const next = { url: '' };
        localStorage.setItem('next', JSON.stringify(next));
        const result = redirectToNext();
        expect(result).toBe(false);
    });

    it('should not redirect and return false if the "next" url equals to the current domain', () => {
        const next = { url: 'http://localhost:3000' };
        localStorage.setItem('next', JSON.stringify(next));
        const result = redirectToNext();
        expect(result).toBe(false);
    });

    it('should redirect and return true if there is a "next" object in localStorage with a non-empty url', () => {
        const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
        const { window } = jsdom;
        global.window = window;
        
        const datacenter = { domain_front: 'https://example.com' };
        const next = { url: 'https://example.com/next' };
        updateObject("datacenter", {data: datacenter});
        updateObject("next", {data: next});
        const result = redirectToNext();
        expect(result).toBe(true);

        delete global.window;
    });
});
