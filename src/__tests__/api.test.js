import {checkRequiredProps, checkRequiredQuery, API} from '../api';
import {makeApiCall} from '../api-call';

jest.mock('../api-call', () => ({
	makeApiCall: jest.fn()
}));

describe('checkRequiredProps', () => {
	it('should throw an error when a required prop is missing', () => {
		const props = {
			name: 'John Doe',
			age: 30
		}
		const fields = ['name', 'address']
		expect(() => checkRequiredProps(props, fields)).toThrow('Error: Missing address in props!')
	})
  
	it('should not throw an error when all required props are present', () => {
		const props = {
			name: 'John Doe',
			address: '123 Main St',
			age: 30
		}
		const fields = ['name', 'address']
		expect(() => checkRequiredProps(props, fields)).not.toThrow()
	})

	it('should not throw an error when no fields are provided', () => {
		const props = {
			name: 'John Doe',
			age: 30
		}
		expect(() => checkRequiredProps(props)).not.toThrow()
	})
})

describe("checkRequiredQuery", () => {
	it("should throw an error if props.query is not defined", () => {
		expect(() => checkRequiredQuery({})).toThrow();
	});
	
	it("should not throw an error if all required parameters are present in props.query", () => {
		expect(() => checkRequiredQuery({query: "name=john&age=30"}, ["name", "age"])).not.toThrow();
	});
	
	it("should throw an error if a required parameter is missing in props.query", () => {
		expect(() => checkRequiredQuery({query: "name=john&age=30"}, ["name", "address"])).toThrow();
	});
});

describe('API', () => {
	test('get_list_url should return correct URL', () => {
		const props = {
			app: 'myapp',
			model: 'mymodel',
			level: 'organization',
			sid: 'mysid',
			serializer: 'list',
			query: 'myquery=test',
			page: 1,
			page_size: 10
		};
		const expectedUrl = '/myapp/mymodel/list/organization/mysid/list/?myquery=test&page=1&page_size=10';
		const actualUrl = API.get_list_url(props);
		expect(actualUrl).toBe(expectedUrl);
	});

	test('get_detail_url should return correct URL', () => {
		const props = {
			app: 'myapp',
			model: 'mymodel',
			sid: 'mysid',
			query: 'myquery=test'
		};
		const expectedUrl = '/myapp/mymodel/detail/mysid/?myquery=test';
		const actualUrl = API.get_detail_url(props);
		expect(actualUrl).toBe(expectedUrl);
	});

	test('post should call makeApiCall with correct arguments', () => {
		const props = {
			data: { a: 1 },
			callback_success: jest.fn(),
			callback_fail: jest.fn()
		};
		API.get_list_url = jest.fn(() => 'myurl');
		const expectedApiCallArgs = {
			login_required: true,
			method: 'post',
			url: 'myurl',
			data: { a: 1 },
			success: props.callback_success,
			fail: props.callback_fail
		};
		API.post(props);
		expect(makeApiCall).toHaveBeenCalledWith(expectedApiCallArgs);
	});

	test('patch should call makeApiCall with correct arguments', () => {
		const props = {
			data: { a: 1 },
			callback_success: jest.fn(),
			callback_fail: jest.fn()
		};
		API.get_detail_url = jest.fn(() => 'myurl');
		const expectedApiCallArgs = {
			login_required: true,
			method: 'patch',
			url: 'myurl',
			data: { a: 1 },
			success: props.callback_success,
			fail: props.callback_fail
		};
		API.patch(props);
		expect(makeApiCall).toHaveBeenCalledWith(expectedApiCallArgs);
	});

	test('get should call makeApiCall with correct arguments', () => {
		const props = {
			url: 'myurl',
			callback_success: jest.fn(),
			callback_fail: jest.fn()
		};
		const expectedApiCallArgs = {
			login_required: true,
			method: 'get',
			url: 'myurl',
			success: props.callback_success,
			fail: props.callback_fail
		};
		API.get(props);
		expect(makeApiCall).toHaveBeenCalledWith(expectedApiCallArgs);
	});

	test('delete should call makeApiCall with correct arguments', () => {
		const props = {
			sid: 'mysid',
			callback_success: jest.fn(),
			callback_fail: jest.fn()
		};
		API.get_detail_url = jest.fn(() => 'myurl');
		const expectedApiCallArgs = {
			login_required: true,
			method: 'delete',
			url: 'myurl',
			success: props.callback_success,
			fail: props.callback_fail
		};
		API.delete(props);
		expect(makeApiCall).toHaveBeenCalledWith(expectedApiCallArgs);
	});
});