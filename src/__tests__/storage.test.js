import {s2es, es2s, Session, Local} from '../storage';

describe('s2es', ()=>{
    it("Test s2es", ()=>{
        expect(s2es("test")).toBe("InRlc3Qi");
    })
})

describe('es2s', ()=>{
    it("Test es2s", ()=>{
        expect(es2s("InRlc3Qi")).toBe("test");
    })
})

describe("Session", ()=>{
    it("test save", ()=>{
        Session.save("label", "value");
        expect(es2s(sessionStorage.getItem(s2es("label")))).toBe("value");
    })

    it("test read", ()=>{
        Session.save("label", "value");
        expect(Session.read("label")).toBe("value");
    })

    it("test remove", ()=>{
        Session.save("label", "value");
        Session.remove("label");
        expect(Session.read("label")).toBe(null);
    })

    it("test clear", ()=>{
        Session.save("label", "value");
        Session.clear();
        expect(Session.read("label")).toBe(null);
    })
})

describe("Local", ()=>{
    it("test save", ()=>{
        Local.save("label", "value");
        expect(es2s(localStorage.getItem(s2es("label")))).toBe("value");
    })

    it("test read", ()=>{
        Local.save("label", "value");
        expect(Local.read("label")).toBe("value");
    })

    it("test remove", ()=>{
        Local.save("label", "value");
        Local.remove("label");
        expect(Local.read("label")).toBe(null);
    })

    it("test clear", ()=>{
        Local.save("label", "value");
        Local.clear();
        expect(Local.read("label")).toBe(null);
    })
})
