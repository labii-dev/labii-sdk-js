import {
    loadScript,
    loadStyle,
    isEmpty,
    hasKey,
    updateDocumentTitle,
    Title,
    truncateString,
    randomID,
    modalOpen,
    modalClose,
    timeSince,
    toJS,
    hideSidenav,
    // toast,
    formatBytes
} from '../utils';
import { render, screen } from '@testing-library/react';

const toast = jest.fn();

describe('updateDocumentTitle', ()=>{
    it("Test updateDocumentTitle", ()=>{
        updateDocumentTitle("test")
        expect(document.title).toBe("Test | Labii");
    })
})

describe('hideSidenav', ()=>{
    it("hideSidenav", ()=>{
        const {container} = render(<div id="main"></div>);
        hideSidenav();
        expect(container.children[0].getAttribute("style")).toBe("padding-left: 0px;");
    })
})

describe('loadScript', ()=>{
    it("Test loadScript", ()=>{
        var url = "https://apis.google.com/js/api.js";
        loadScript(url, ()=>{
            // do nothing
        })
        expect(document.querySelector(`[src="${url}"]`)).toBeTruthy();
    })
})

describe('loadStyle', ()=>{
    it("Test loadStyle", ()=>{
        var url = "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css";
        loadStyle(url);
        expect(document.querySelector(`[href="${url}"]`)).toBeTruthy();
    })
})

describe('modalOpen', ()=>{
    it("Test modalOpen", ()=>{
        // render(
        //     <div id="modal1" class="modal">
        //       <div class="modal-content">
        //         <h4>Modal Header</h4>
        //         <p>A bunch of text</p>
        //       </div>
        //       <div class="modal-footer">
        //         <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
        //       </div>
        //     </div>
        // );
        // modalOpen("modal1");
    })
})

describe('modalClose', ()=>{
    it("Test modalClose", ()=>{
        // not ready
    })
})

describe('isEmpty', ()=>{
    it("Test isEmpty", ()=>{
        expect(isEmpty("")).toBeTruthy();
        expect(isEmpty(null)).toBeTruthy();
        expect(isEmpty(undefined)).toBeTruthy();
        expect(isEmpty([])).toBeTruthy();
        expect(isEmpty({})).toBeTruthy();
        expect(isEmpty("dd")).toBe(false);
    })
})

describe('hasKey', ()=>{
    it("Test hasKey", ()=>{
        expect(hasKey("", "test")).toBe(false);
        expect(hasKey({test: "data"}, "test")).toBe(true);
    })
})

describe('Title', ()=>{
    it("Test Title", ()=>{
        expect(Title("title")).toBe("Title");
    })
})

describe('truncateString', ()=>{
    it("Test truncateString", ()=>{
        expect(truncateString("title", 10)).toBe("title");
        expect(truncateString("title", 1)).toBe("t...");
    })
})

describe('randomID', ()=>{
    it("Test randomID", ()=>{
        expect(randomID().length).toBe(19);
    })
})

describe('timeSince', ()=>{
    it("Test timeSince", ()=>{
        expect(timeSince(new Date())).toBe("1 second ago");
    })
})

describe('toJS', ()=>{
    it("Test toJS", ()=>{
        var data = {test: true};
        expect(toJS(data).test).toBe(data.test);
    })
})

describe('toast', ()=>{
    it("Test toast", ()=>{
        toast("test");
        // expect(screen.getByText("test")).toBeInTheDocument();
    })
})

describe('dropdownInit', ()=>{
    it("Test dropdownInit", ()=>{
        // not ready
    })
})

describe('formatBytes', ()=>{
    it("Test formatBytes", ()=>{
        expect(formatBytes(0)).toBe("0 Bytes");
        expect(formatBytes(10)).toBe("10 Bytes");
        expect(formatBytes(2000)).toBe("1.95 KB");
        expect(formatBytes(2000000)).toBe("1.91 MB");
        expect(formatBytes(2000000000)).toBe("1.86 GB");
    })
})
