import {APIResource} from '../sdk';
import {API} from '../api';
import {Title} from '../utils';

jest.mock('../api-call', () => ({
	makeApiCall: jest.fn()
}));

describe('APIResource', () => {
	const props = {
		app: 'myapp',
		model: 'mymodel',
		level: 'organization',
		sid: 'mysid',
		serializer: 'list',
		query: 'myquery=test',
		page: 1,
		page_size: 10,
		data: {
			name: 'John Doe',
			age: 26,
		}
	};

	it('should call create function', () => {
		const spy = jest.spyOn(API, 'post');
		APIResource.create(props);
		expect(spy).toHaveBeenCalled();
		expect(spy).toHaveBeenCalledWith(props);
	});

	it('should call list function with all pages', () => {
		const spy = jest.spyOn(API, 'get');
		APIResource.list({ ...props, all_pages: true });
		expect(spy).toHaveBeenCalled();
		// add more expectation here
	});

	it('should call retrieve function', () => {
		const spy = jest.spyOn(API, 'get');
		APIResource.retrieve(props);
		expect(spy).toHaveBeenCalled();
		expect(spy).toHaveBeenCalledWith({
			...props,
			url: API.get_detail_url(props),
		});
	});

	it('should call modify function', () => {
		const spy = jest.spyOn(API, 'patch');
		APIResource.modify(props);
		expect(spy).toHaveBeenCalled();
		expect(spy).toHaveBeenCalledWith({
			...props,
			url: API.get_detail_url(props),
		});
	});

	it('should call delete function', () => {
		const spy = jest.spyOn(API, 'delete');
		APIResource.delete(props);
		expect(spy).toHaveBeenCalled();
		expect(spy).toHaveBeenCalledWith({
			...props,
			url: API.get_detail_url(props),
			callback_success: expect.any(Function),
			callback_fail: expect.any(Function),
		});
	});
});

describe('Labii SDK', () => {
	const tables = {
		sample: {
			name_singular: "sample",
			app: "labii",
			should_hide_delete_button: true
		},
		signers: {
			name_singular: "signer",
			app: "labii"
		},
		invoice: {
			name_singular: "invoice",
			app: "labii"
		}
	};

	it('should create an instance of Labii SDK', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {};
			expect(Labii).toBeDefined();
		});
	});
  
	it('should create a resource using the create method', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {
				create: jest.fn(),
				list: jest.fn(),
				retrieve: jest.fn(),
				modify: jest.fn(),
				delete: jest.fn()
			};
			const props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			Labii[name].create(props);
			expect(Labii[name].create).toHaveBeenCalledWith(props);
		});
	});
  
	it('should retrieve a resource using the retrieve method', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {
				create: jest.fn(),
				list: jest.fn(),
				retrieve: jest.fn(),
				modify: jest.fn(),
				delete: jest.fn()
			};
			const props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			Labii[name].retrieve(props);
			expect(Labii[name].retrieve).toHaveBeenCalledWith(props);
		});
	});
  
	it('should modify a resource using the modify method', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {
				create: jest.fn(),
				list: jest.fn(),
				retrieve: jest.fn(),
				modify: jest.fn(),
				delete: jest.fn()
			};
			const props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			Labii[name].modify(props);
			expect(Labii[name].modify).toHaveBeenCalledWith(props);
		});
	});
  
	it('should list resources using the list method', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {
				create: jest.fn(),
				list: jest.fn(),
				retrieve: jest.fn(),
				modify: jest.fn(),
				delete: jest.fn()
			};
			const props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			Labii[name].list(props);
			expect(Labii[name].list).toHaveBeenCalledWith(props);
		});
	});

	it('should delete resources using the delete method', () => {
		const Labii = {};
		Object.keys(tables).forEach((table) => {
			const name = Title(tables[table].name_singular);
			Labii[name] = {
				create: jest.fn(),
				list: jest.fn(),
				retrieve: jest.fn(),
				modify: jest.fn(),
				delete: jest.fn()
			};
			const props = {
				app: tables[table].app,
				model: tables[table].name_singular
			};
			Labii[name].delete(props);
			expect(Labii[name].delete).toHaveBeenCalledWith(props);
		});
	});
})