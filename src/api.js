import {makeApiCall, makeApiCallAsync} from './api-call';
import {hasKey, toast} from './utils';
import {updateURL} from './url-query';
import {getObject} from './local-store';

/**
 * check if the fields exists in props
 */
export const checkRequiredProps = (props, fields=[])=>{
    fields.forEach((field)=>{
        if (!Object.keys(props).includes(field)){
            throw `Error: Missing ${field} in props!`;
        }
    })
}

/**
 * check if query contaisn certain parameters
 */
export const checkRequiredQuery = (props, parameters=[])=>{
    checkRequiredProps(props, ["query"]);
    parameters.forEach((parameter)=>{
        if (!props.query.includes(`${parameter}=`)){
            throw `Missing ${parameter} in query!`;
        }
    })
}

export const API = {
    get_list_url: (props)=>{
		const organization = getObject("organization");
        var default_props = {
            level: "organization",
            sid: organization.sid,
            serializer: "list"
        };
        props = {...default_props, ...props};
        checkRequiredProps(props, ["app", "model"]);
        var url = `/${props.app}/${props.model}/list/${props.level}/${props.sid}/${props.serializer}/${props.query && props.query !== "" ? "?" : ""}${props.query && props.query !== "" ? props.query : ""}`;
		// existing url
		if (props.url && props.url !== ""){
			url = props.url;
		}
        ["page", "page_size"].forEach((item)=>{
            if (hasKey(props, item)){
                url = updateURL(url, {[item]: props[item]});
            }
        });
        return url;
    },
    get_detail_url: (props)=>{
		if (props.url && props.url !== ""){
			return props.url;
		}
        checkRequiredProps(props, ["app", "model", "sid"]);
        return (
            `/${props.app}/${props.model}/detail/${props.sid}/${props.query && props.query !== "" ? "?" : ""}${props.query && props.query !== "" ? props.query : ""}`
        );
    },
    post: (props)=>{
        checkRequiredProps(props, ["data"]);
        makeApiCall({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "post",
            url: API.get_list_url({
                ...props,
                serializer: "detail"
            }),
            data: props.data,
            success: props.callback_success ? props.callback_success : null,
            fail: props.callback_fail ? props.callback_fail : (err)=>{
                toast(err);
            }
        });
    },
    post_async: async (props)=>{
        checkRequiredProps(props, ["data"]);
		const response = await makeApiCallAsync({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "post",
            url: API.get_list_url({
                ...props,
                serializer: "detail"
            }),
            data: props.data,
		})
		return response;
    },
    patch: (props)=>{
        checkRequiredProps(props, ["data"]);
        makeApiCall({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "patch",
            url: API.get_detail_url(props),
            data: props.data,
            success: props.callback_success ? props.callback_success : null,
            fail: props.callback_fail ? props.callback_fail : (err)=>{
                toast(err);
            }
        });
    },
	patch_async: async (props)=>{
        checkRequiredProps(props, ["data"]);
		const response = await makeApiCallAsync({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "patch",
            url: API.get_detail_url(props),
            data: props.data,
		})
		return response;
    },
    get: (props)=>{
        checkRequiredProps(props, ["url"]);
        makeApiCall({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "get",
            url: props.url,
            success: props.callback_success ? props.callback_success : null,
            fail: props.callback_fail ? props.callback_fail : (err)=>{
                toast(err);
            }
        });
    },
	get_async: async (props)=>{
        checkRequiredProps(props, ["url"]);
		const response = await makeApiCallAsync({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "get",
            url: props.url,
		})
		return response;
    },
    delete: (props)=>{
        makeApiCall({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "delete",
            url: API.get_detail_url(props),
            success: props.callback_success ? props.callback_success : null,
            fail: props.callback_fail ? props.callback_fail : (err)=>{
                toast(err);
            }
        });
    },
	delete_async: async (props)=>{
		const response = await makeApiCallAsync({
            login_required: Object.keys(props).includes("login_required") ? props.login_required : true,
            method: "delete",
            url: API.get_detail_url(props),
		})
		return response;
    },
}
