import { Buffer } from 'buffer';
import {getObject} from './local-store';

export const s2es = (s)=>{
    var es = Buffer.from(JSON.stringify(s)).toString('base64');
    return es.split("=").join("_");
}

export const es2s = (es)=>{
    return JSON.parse(Buffer.from(es.split("_").join("="), 'base64').toString());
}

export const Session = {
    save: (label, value) =>{
        try{
            sessionStorage.setItem(s2es(label), s2es(value));
            return true;
        }
        catch(err){
            // eslint-disable-next-line no-console
            console.log(`Session storage (${label}) exceeded the quota!`);
            return false;
        }
    },
    read: (label) =>{
        if (sessionStorage.getItem(s2es(label)) === null){
            return null;
        }
        else{
            return es2s(sessionStorage.getItem(s2es(label)));
        }
    },
    remove: (label)=>{
        sessionStorage.removeItem(s2es(label));
    },
    clear: ()=>{
        sessionStorage.clear();
    }
}

export const Local = {
    save: (label, value)=>{
        try{
            localStorage.setItem(s2es(label), s2es(value));
            return true;
        }
        catch(err){
            // eslint-disable-next-line no-console
            console.log(`Local storage (${label}) exceeded the quota!`);
			console.log(err);
            return false;
        }
    },
    read: (label)=>{
        if (localStorage.getItem(s2es(label)) === null){
            return null;
        }
        else{
            return es2s(localStorage.getItem(s2es(label)));
        }
    },
    remove: (label)=>{
        localStorage.removeItem(s2es(label));
    },
    clear: ()=>{
        localStorage.clear();
    }
}

const getIndexedDB = async () =>{
	const request = indexedDB.open("labii", 1);
	request.onerror = () => {
		throw new Error('Failed to open database');
	};
	request.onupgradeneeded = (event) => {
		const db = event.target.result;
		if (!db.objectStoreNames.contains('api')) {
			db.createObjectStore('api', { keyPath: 'id' });
		}
	};
	// Wait for the database to be opened or created
	const db = await new Promise((resolve) => {
		request.onsuccess = () => resolve(request.result);
	});
	return db;
}

const getExpirationDate = (duration) =>{
	const current_date = new Date();
	const duration_parts = duration.match(/(\d+)([d|m|y])/);
	const amount = parseInt(duration_parts[1]);
	const unit = duration_parts[2];
	let new_date;
	switch (unit) {
		case 'd':
			new_date = new Date(current_date.getTime() + (amount * 24 * 60 * 60 * 1000));
			break;
		case 'm':
			new_date = new Date(current_date.getTime() + (amount * 30 * 24 * 60 * 60 * 1000));
			break;
		case 'y':
			new_date = new Date(current_date.getTime() + (amount * 365 * 24 * 60 * 60 * 1000));
			break;
		default:
			throw new Error(`Invalid duration unit: ${unit}`);
	}
	const year = new_date.getFullYear();
	const month = new_date.getMonth() + 1; // getMonth() is zero-based
	const day = new_date.getDate();
	return `${year.toString().padStart(4, '0')}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`;
}

export const IndexedDB = {
	save: async (label, value, date_expiration="1d")=>{
		const datacenter = getObject("datacenter");
		if (datacenter.is_debug){
			// eslint-disable-next-line no-console
			console.log(`IndexedDB Write: ${label}`); // for debug
		}
		// Open the IndexedDB database
		const db = await getIndexedDB();
		// Start a transaction on the object store
		const tx = db.transaction('api', 'readwrite');
		const store = tx.objectStore('api');
		// get date
		var date = "";
		if (date_expiration !== ""){
			date = getExpirationDate(date_expiration);
		}
		// Add the ID and value to the object store
		store.put({ id: s2es(label), value: s2es(value), date_expiration: date});
		// Close the transaction and the database connection
		await tx.complete;
		db.close();
	},
	read: async (label)=>{
		const datacenter = getObject("datacenter");
		if (datacenter.is_debug){
			// eslint-disable-next-line no-console
			console.log(`IndexedDB Read: ${label}`); // for debug
		}
		// Open or create the IndexedDB database
		const db = await getIndexedDB();
		// Perform a transaction on the object store
		const tx = db.transaction('api', 'readonly');
		const store = tx.objectStore('api');
		// Retrieve the value based on the provided ID
		const request = store.get(s2es(label));
		let value = undefined;
		request.onsuccess = () => {
			const result = request.result;
			// Return the value
			if (result){// key is not exists
				if (result.date_expiration !== ""){
					const today = new Date();
					const dateParts = result.date_expiration.split("-");
					const date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
					if (date.getTime() >= today.getTime()) {
						value = es2s(result.value);
					}
					else{
						try{
							store.delete(s2es(label));
						}
						catch{}
					}
				}
				else{
					value = es2s(result.value);
				}
			}
		};
		await new Promise(resolve => {
			tx.oncomplete = resolve;
		});
		// Close the transaction and the database connection
		tx.complete;
		db.close();
		return value;
	},
	remove: async (label)=>{
		const datacenter = getObject("datacenter");
		if (datacenter.is_debug){
			// eslint-disable-next-line no-console
			console.log(`IndexedDB Remove: ${label}`); // for debug
		}
		// Open the IndexedDB database
		const db = await getIndexedDB();
		// Start a transaction on the object store
		const tx = db.transaction('api', 'readwrite');
		const store = tx.objectStore('api');
		// Add the ID and value to the object store
		store.delete(s2es(label));
		// Close the transaction and the database connection
		await tx.complete;
		db.close();
	}
}