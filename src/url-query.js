/**
 * parse query to json object
 */
export const query2json = (query)=>{
	if (!query){
		return {};
	}
	if (!query.includes("next=")){
		query = query.split("#")[0];
	}
	query = decodeURIComponent(query.replace("?", ""));
	var json = {};
	if (query !== "" && query !== null){
		var queries = query.split("&");
		if (query.includes("next=")){
			json["next"] = query.split("next=")[1].replace("__QS__", "?");
			queries = query.split("next=")[0].split("&");
		}
		queries.forEach((q)=>{
			if (q.includes("=")){
				var match = q.split("=");
				if (match[1] === "true"){
					json[match[0]] = true;
				}
				else if (match[1] === "false"){
					json[match[0]] = false;
				}
				else{
					json[match[0]] = match[1];
				}
			}
		});
	}
	return json;
}

/**
 * function to convert json to query
 */
export const json2query = (json)=>{
	var query = "";
	var next = "";
	Object.keys(json).forEach((k, i)=>{
		if (json[k] !== ""){
			if (k === "next"){
				next = json[k];
			}
			else{
				let data = json[k];
				if (typeof data === "object"){
					data = encodeURIComponent(JSON.stringify(json[k]));
				}
				if (i === 0){
					query = `${k}=${data}`;
				}
				else{
					query = `${query}&${k}=${data}`;
				}
			}
		}
	});
	if (next !== ""){
		query = `${query}${query === "" ? "" : "&"}next=${next}`;
	}
	if (query.startsWith("&")) {// remove the first &
		query = query.slice(1);
	}
	return query;
}

/**
 * return the string query from url
 */
export const getURLQuery = ()=>{
	if (!window.location.href.includes("?")){
		return "";
	}
	else{
		return window.location.href.split("#")[0].split("?")[1];
	}
}

/**
 * return the string query from url
 */
export const getURLQueryJson = ()=>{
	if (!window.location.href.includes("?")){
		return {};
	}
	else{
		let url = window.location.href;
		url = url.replace(/\?/g, "__QS__").replace("__QS__", "?");
		return query2json(url.split("?")[1]);
	}
}

export const updateQuery = (query, json_query)=>{
	if (Object.keys(json_query).length === 0){
		return query;
	}
	else{
		var j = query2json(query);
		j = {...j, ...json_query};
		return json2query(j);
	}
}

/**
 * update the get options for a url
 * str url, the url to update
 * json_query, json_query, the additional value to add
 * return url
 */
export const updateURL = (url, json_query)=>{
	url = url.split("#")[0];
	if (Object.keys(json_query).length === 0){
		return url;
	}
	else{
		var query = "";
		if (url.includes("?")){
			query = url.split("?")[1];
		}
		query = updateQuery(query, json_query);
		return `${url.split("?")[0]}?${query}`;
	}
}

/**
 * encode url
 */
export const encodeURL = (url) =>{
	return encodeURI(url).replace("@", "%40");
}

/**
 * convert form data to query
 * data as dict
 * returned value assigned to bHN6R2RhdGFud0ZP
 *
 */
export const formData2Query = (data)=>{
	var json = {};
	Object.keys(data).forEach((field)=>{
		if (typeof data[field] === "string" || typeof data[field] === "number"){
			json[field] = data[field];
		}
		else if (data[field] === null || data[field] === undefined){
			json[field] = "";
		}
		else{
			json[field] = JSON.stringify(data[field]).replaceAll('"', "'");
		}
	});
	// replace value
	var to_be_replaced = {
		"%": "%25",
		"?": "%3F",
		"&": "and",
		"=": "__EQ__",
		"'": "%27",
		'"': "%22"
	}
	var query = JSON.stringify(json);
	Object.keys(to_be_replaced).forEach((field)=>{
		query = query.replaceAll(field, to_be_replaced[field])
	})
	return query;
}

/**
 * convert value of bHN6R2RhdGFud0ZP to dict
 */
export const queryData2FormData = (jsonString)=>{
	var to_be_replaced = {
		"__EQ__": "="
	}
	Object.keys(to_be_replaced).forEach((field)=>{
		jsonString = jsonString.replaceAll(field, to_be_replaced[field])
	})
	var data = JSON.parse(jsonString);
	Object.keys(data).forEach((field)=>{
		if (typeof data[field] === 'string' && data[field].includes("{") && !data[field].includes("{{")){
			try{
				data[field] = JSON.parse(data[field].replaceAll("'", '"'));
			}
			catch(err){
				data[field] = err;
			}
		}
	})
	return data;
}
