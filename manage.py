""" This script is used to manage the project."""
import sys
import os
import re
import json
from labii_sdk_core.sdk import print_green, print_blue, print_yellow, update_version_in_package, get_version, prepare_usage
from labii_sdk_core.whl import install_whl
from labii_sdk_core.git import merge_requests

def read_tables():
	"""Read the tables from the frontend and save them to a JSON file."""
	with open("../labii-frontend/src/components/common/Tables.js", "r", encoding="utf8") as file_handler:
		lines = file_handler.readlines()
	# Initialize an empty string for the JSON string
	json_str = ''
	# The keys we are interested in
	keys = ['name_plural', 'name_singular', 'app: ', 'should_hide_delete_button']
	# Initialize a flag to indicate whether we are inside the Tables variable
	inside_tables = False
	# Initialize a flag to indicate whether we are inside a first level object
	inside_first_level_object = False
	inside_array = False
	# Iterate over the lines
	for line in lines:
		# If we are inside the Tables variable
		if inside_tables:
			# Remove comments
			line = line.split('//')[0].strip()
			# If the line contains a first level object key or any of the keys we are interested in
			if not "i18n" in line:
				if ': {' in line or ':{' in line:
					# Add quotes around keys
					line = re.sub(r'(\w+):', r'"\1":', line)
					# Append the line to the JSON string
					json_str += line
					inside_first_level_object = True
				elif inside_first_level_object:
					if '[' in line and not "]" in line:
						inside_array = True
					elif "]" in line and inside_array and not '[' in line:
						inside_array = False
					# If the line contains any of the keys we are interested in
					if any(key in line for key in keys):
						# Add quotes around keys
						line = re.sub(r'(\w+):', r'"\1":', line)
						# Append the line to the JSON string
						json_str += line
					elif '},' in line and inside_array is False:
						json_str += line
						inside_first_level_object = False
			# If the line ends with a semicolon, we are done
			if line.endswith(';'):
				break
		# If the line starts with 'Tables', we are starting the Tables variable
		elif line.strip().startswith('export const Tables'):
			inside_tables = True
	# Remove the trailing semicolon from the JSON string
	json_str = "{" + json_str[:-1] + "}}"
	# Convert the JSON string to a Python object
	# Remove trailing commas
	json_str = re.sub(r',\s*}', '}', json_str)
	json_str = re.sub(r',\s*]', ']', json_str)
	print(json_str)
	tables = json.loads(json_str)
	tables_json = {}
	for table in tables:
		table_json = {}
		if "name_plural" in tables[table]:
			table_json["name_plural"] = tables[table]["name_plural"]
		if "name_singular" in tables[table]:
			table_json["name_singular"] = tables[table]["name_singular"]
		if "app" in tables[table]:
			table_json["app"] = tables[table]["app"]
		if "should_hide_delete_button" in tables[table]:
			table_json["should_hide_delete_button"] = tables[table]["should_hide_delete_button"]
		tables_json[table] = table_json
	with open("./src/tables.json", "w", encoding="utf8") as file_handler:
		json.dump(tables_json, file_handler, indent=4)

def main():
	"""Main function."""
	name = "sdk"
	actions = ["read_tables"]
	# update here for arguments
	usage = prepare_usage(actions)
	usage = f"{usage}\nread_tables, update tables.json"

	if len(sys.argv) < 2:
		print_blue(usage)
		sys.exit()
	action = sys.argv[1]
	if action == "merge":
		version = merge_requests()
		if version != "":
			update_version_in_package(version)
	elif action == "install_whl":
		install_whl("labii_sdk_core")
	elif action == "version":
		print_yellow(get_version())
	elif action == "build":
		version = merge_requests()
		if version != "":
			update_version_in_package(version)
		os.system("npm run build")
		os.system("git add .")
		os.system("git commit -m 'Added the newly build dist.'")
	elif action == "read_tables":
		read_tables()
	print_green("Done!")
	print_blue(usage)

if __name__ == "__main__":
	main()
