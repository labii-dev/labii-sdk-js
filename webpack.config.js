const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = {
	mode: 'production',
	entry: './index.js',
	output: {
		path: path.resolve('dist'),
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				use: 'babel-loader',
			},
		],
	},
	resolve: {
		extensions: ['.js'],
		fallback: {
			"buffer": require.resolve("buffer/")
		}
	},
};